# CryptoBlender

Easy way to implement AES, DES, BlowFish, RC4 cryptographic algorithms and SHA-1, SHA-256, SHA-512, MD5 as well. Security is very important and bad implementation of cryptographic algorithms is one of the most common security problem. 

![](screenshots/cryptoblenderlogo1.png)


#Examples
--------
####AES
```
Blender aes = new CryptoBlender().getAlgorithm("AES"); // initialize (AES, DES, Blowfish, RC4)
ase.generateKey(128); // set key size and generate it
aes.setKey(somekey); // or you can set your own existing key

byte[] ciphertext = aes.encryption(text); // encryption
aes.decryption(ciphertext); // decryption

// other options
aes.getCipher(); // get Cipher
aes.getKey(); // get key value
aes.getSpecification(); // print specification
aes.keySize(); // get key size

```

####DES

```
Blender des = new CryptoBlender().getAlgorithm("DES"); // initialize (AES, DES, Blowfish, RC4)

```
####SHA and MD5
```
byte[] sha1 = BlenderSha.sha(text, "SHA-1");
byte[] sha256 = BlenderSha.sha(text, "SHA-256");
byte[] sha512 = BlenderSha.sha(text, "SHA-512");
byte[] md5 = BlenderSha.sha(text, "MD5");
```
![](screenshots/Screen Shot 2015-10-22 at 6.59.29 PM.png)


#License
--------

    Copyright 2015 Milan Adamovic.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
