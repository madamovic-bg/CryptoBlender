
import algoritms.BlenderSha;
import algoritms.CryptoBlender;
import interfaces.Blender;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Milan
 */
public class Main {

   public static void main(String[] args) throws NoSuchAlgorithmException {
       
       String text = "The Oracle Corporation is an American global computer technology corporation, headquartered in Redwood City, California. "; // some text
       
       
       /* RC4
       Blender rc4 = new CryptoBlender().getAlgorithm("rc4");
       rc4.generateKey(128);
       
       byte[] ciphertext = rc4.encryption(text);
       System.out.println(Arrays.toString(ciphertext));
       System.out.println(rc4.decryption(ciphertext));
       
       System.out.println(rc4.keySize());
       
       
       /* BlowFish
       Blender blowFish = new CryptoBlender().getAlgorithm("Blowfish"); // inicialize
       
       blowFish.generateKey(128); // generate key
       blowFish.setKey(null);  // or set key
       
       byte[] ciphertext = blowFish.encryption(text); // encryption
       blowFish.decryption(ciphertext); // decryption
       
       blowFish.getCipher(); // get cipher
       blowFish.getKey(); // get key
       blowFish.getSpecification(); // print specification
       blowFish.keySize(); // get key size
               
       System.out.println(blowFish.getCipher().getMaxAllowedKeyLength("Blowfish"));
       
       // DES
       Blender des = new CryptoBlender().getAlgorithm("DES"); // inicialize
       Blender aes = new CryptoBlender().getAlgorithm("AES"); // inicialize
       
       des.generateKey(56); // generate key
       des.setKey(null); // or set key
       
       byte[] ciphertext = des.encryption(text); // encryption
       des.decryption(ciphertext); // decryption
       
       des.getCipher(); // get cipher
       des.getKey(); // get key
       des.getSpecification(); // print specification
       des.keySize(); // get key size
       
       
       // AES
       Blender aes = new CryptoBlender().getAlgorithm("AES"); // inicialize
       aes.generateKey(128); // generate key
       aes.setKey(null); // or set key
       
       byte[] ciphertext = aes.encryption(text); // encryption
       aes.decryption(ciphertext); // decryption
       
       aes.getCipher(); // get cipher
       aes.getKey(); // get key
       aes.getSpecification(); // print specification
       aes.keySize(); // get key size
       

       
       System.out.println("SHA-1 : " + Arrays.toString(BlenderSha.sha(text, "SHA-1")));
       System.out.println("SHA-256 : " + Arrays.toString(BlenderSha.sha(text, "SHA-256")));
       System.out.println("SHA-512 : " + Arrays.toString(BlenderSha.sha(text, "SHA-512")));
       System.out.println("MD5 : " + Arrays.toString(BlenderSha.sha(text, "MD5")));*/
       
   }
    
}
