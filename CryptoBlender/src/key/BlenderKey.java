/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package key;

import algoritms.AES;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 *
 * @author Milan
 */
public class BlenderKey {
    
    
     public static SecretKey generateKey(int size, String algorithm){
         try {
            KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
            keyGen.init(size);
            return keyGen.generateKey();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     }
     
     
}
