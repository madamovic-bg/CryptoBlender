/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritms;

import java.security.MessageDigest;

/**
 *
 * @author Milan
 */
public class BlenderSha {
    public static final String TAG = "SHA : ";
    
    /** SHA (Secure Hash Algorithm) is one-way function.
     * There move version of SHA like SHA-1 (128), SHA-256 (256), SHA-512(512)
     * @param alg 
     * @param text 
     * @return byte[]
     */
    public static byte[] sha(String text, String alg){
        try{
            MessageDigest messageDigest = MessageDigest.getInstance(alg);
            messageDigest.reset();
            messageDigest.update(text.getBytes());
            return messageDigest.digest();
        }catch(Exception e){
            System.out.println(TAG + e.getMessage());
        }
        return null;
    }
    
    
}
