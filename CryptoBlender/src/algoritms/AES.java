/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritms;

import interfaces.Blender;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import key.BlenderKey;

/**
 *
 * @author Milan
 */
public class AES implements Blender{
    private static final String TAG = "AES : ";
    private SecretKey secretKey;
    private Cipher cipher;
    
    public AES(){
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    public byte[] encryption(String text) {
        try{
            if(text == null || secretKey == null)
                return null;
            
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, cipher.getParameters());
            return cipher.doFinal(text.getBytes());
            
        }catch(InvalidKeyException | IllegalBlockSizeException | BadPaddingException e){
            System.out.println(TAG + e.getMessage());
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

  
    @Override
    public String decryption(byte[] ciphertext) {
        try{
            if(ciphertext == null || secretKey == null)
                return null;
            
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, cipher.getParameters());
            byte[] dencryption = cipher.doFinal(ciphertext);
            
            return new String(dencryption, "UTF8");
        }catch(IllegalBlockSizeException | BadPaddingException e){
            System.out.println(TAG + e.getMessage());
        } catch (UnsupportedEncodingException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(AES.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    
    @Override
    public int keySize() {
        return secretKey.getEncoded().length;
    }

    @Override
    public void setKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    
    @Override
    public void generateKey(int size) {
        secretKey = BlenderKey.generateKey(size, "AES");
    }

    
    @Override
    public SecretKey getKey() {
        return secretKey;
    }

    
    @Override
    public void getSpecification() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(cipher.getAlgorithm());
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(cipher.getBlockSize());
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(cipher.getProvider());
        stringBuilder.append(System.getProperty("line.separator"));

        System.out.println(stringBuilder);
    }

    
    @Override
    public Cipher getCipher() {
        return cipher;
    }

    
    
    
    
    
    
    
}
