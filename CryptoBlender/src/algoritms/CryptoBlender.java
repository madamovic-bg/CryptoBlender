/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritms;

import interfaces.Blender;

/**
 *
 * @author Milan
 */
public class CryptoBlender {
    
    public Blender getAlgorithm(String name){
        if(name == null)
            return null;
        
        if(name.equalsIgnoreCase("AES"))
            return new AES();
            
        if(name.equalsIgnoreCase("DES"))
            return new DES();
        
        if(name.equalsIgnoreCase("Blowfish"))
            return new BlowFish();
        
        if(name.equalsIgnoreCase("RC4"))
            return new RC4();
        
        return null;
    }
    
}
