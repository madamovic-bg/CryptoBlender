/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritms;

import interfaces.Blender;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import key.BlenderKey;

/**
 *
 * @author Milan
 */
public class RC4 implements Blender{
    private static final String TAG = "RC4 : ";
    private SecretKey secretKey;
    private Cipher cipher;
    

    public RC4(){
        try {
            cipher = Cipher.getInstance("RC4");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(RC4.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    public byte[] encryption(String text) {
        try{
            if(text == null || secretKey == null)
                return null;
        
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "RC4");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            return cipher.doFinal(text.getBytes());
            
        }catch(InvalidKeyException | IllegalBlockSizeException | BadPaddingException e){
            System.out.println(TAG + e.getMessage());
        }
        return null;
    }

    @Override
    public String decryption(byte[] ciphertext) {
        try{
            if(ciphertext == null || secretKey == null)
                return null;
            
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "RC4");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] dencryption = cipher.doFinal(ciphertext);
            
            return new String(dencryption, "UTF8");
        }catch(InvalidKeyException e){
            System.out.println(TAG + e.getMessage());
        } catch (IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException ex) {
            Logger.getLogger(RC4.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int keySize() {
        return secretKey.getEncoded().length;
    }

    @Override
    public void setKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public SecretKey getKey() {
        return secretKey;
    }

    @Override
    public void generateKey(int size) {
        secretKey = BlenderKey.generateKey(size, "RC4");
    }

    @Override
    public void getSpecification() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(cipher.getAlgorithm());
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(cipher.getBlockSize());
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(cipher.getProvider());
        stringBuilder.append(System.getProperty("line.separator"));

        System.out.println(stringBuilder);
    }

    @Override
    public Cipher getCipher() {
        return cipher;
    }

    
    
}
