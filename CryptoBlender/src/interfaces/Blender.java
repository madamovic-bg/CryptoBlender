/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.io.File;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;

/**
 *
 * @author Milan
 */
public interface Blender {
    /** Encryption method
     @param text 
     @return byte[]
     */
    byte[] encryption(String text);
    
    /** Decryption method
     @param ciphertext
     * @return byte[] */
    String decryption(byte[] ciphertext);
    
    /** Get key size
     * @return String*/
    int keySize();
    /** Set key
     @param secretKey */
    void setKey(SecretKey secretKey);
    /** Get Key value
     @return SecretKey */
    SecretKey getKey();
    void generateKey(int size);
    /** Get Specification
     Cipher regime
     Block size
     Provider*/
    void getSpecification();
    /** Get Cipher
     @return Cipher*/
    Cipher getCipher();
}
